//menu header
$('.header-control').on('click', function () {
	$(".header-nav").addClass("on");
	$(".header").addClass('style');
	
	var h_mv = $('.sec-mv').outerHeight();
	if ($(window).scrollTop() >= h_mv) {
		if ($('.header-nav').hasClass('on')) {
			$('html').addClass('no-scroll');
		} else {
			$('html').removeClass('no-scroll');
		}
	} else {
		$('html').removeClass('scroll');
	}
});
$('.header-nav .control').on('click', function () {
	$(".header-nav").removeClass("on");
	$('html').removeClass('no-scroll');
	$(".header").removeClass('style');
});
$(window).on("load resize", function () {
	var h_mv = $('.sec-mv').outerHeight();
	$('.header').css({'top': h_mv});
});
$(window).on("load scroll", function () {
	var h_mv = $('.sec-mv').outerHeight();
	$('html').removeClass('scroll');
	if ($(window).scrollTop() >= h_mv) {
		$('.header').addClass('fixed');
		
		if ($('.header-nav').hasClass('on')) {
			$('html').addClass('no-scroll');
		} else {
			$('html').removeClass('no-scroll');
		}
	} else {
		$('.header').removeClass('fixed');
	}
});


//backtop
jQuery(document).ready(function ($) {
	$(".backtop").hide();
	$(window).on("scroll", function () {
		if ($(this).scrollTop() > 100) {
			$(".backtop").fadeIn("fast");
		} else {
			$(".backtop").fadeOut("fast");
		}
		
		var ofsetFooter = $('.footer').offset().top;
		if ($(this).scrollTop() + $(this).outerHeight() >= ofsetFooter) {
			$(".backtop").removeClass('fixed');
		} else {
			$(".backtop").addClass('fixed');
		}
	});
	$('.backtop').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 500);
		return false;
	});
});

// link anchor
if (window.location.hash)
	scroll(0, 0);
setTimeout(function () {
	scroll(0, 0);
}, 1);

$(function () {
	if (window.location.hash) {
		$('html,body').animate({
			scrollTop: $(window.location.hash).offset().top
		}, 800);
	}
});
$(function () {
	$('.link[href^="#"]').click(function () {
		var speed = 800;
		var href = jQuery(this).attr("href");
		var target = jQuery(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop: position}, speed, 'swing');
		
		$(".header-nav").removeClass("on");
		$('html').removeClass('no-scroll');
		
		return false;
	});
});


$(window).on('scroll load assessFeatureHeaders', function () {
	var scrollTop = $(window).scrollTop();
	var appearenceBuffer = 60;
	var windowBottom = scrollTop + $(window).height() - appearenceBuffer;
	$('body').toggleClass('scrolled-down', scrollTop > 0);
	$('.effect01:not(.on), .effectScale:not(.on)').filter(function () {
		var offset = $(this).offset().top;
		var height = $(this).outerHeight();
		return offset + height >= scrollTop && offset <= windowBottom;
	}).addClass('on');
});
